typedef struct HL_Command_s
{
	HL_Command_s* m_pNext;
	char* m_pszName;
	void* m_pFunction; // void* or intptr_t suffices for our purposes
	intptr_t m_iUnknown; // this is not necessary, we could also replace it with char m_Padding[4]; as well, it's just 0, so maybe it's a version indicator?
}HL_Command_t;

typedef struct HL_UserMsg_s
{
	char m_Padding[8];
	char m_szUserMsg[15];
	char m_Unknown; // version of some kind?
	HL_UserMsg_s* m_pNext;
	void* m_pFunction;
}HL_UserMsg_t;

typedef HL_Event_s
{
	HL_Event_s* m_pNext;
	char* m_pszName;
	void* m_pFunction;
	char m_Padding[4];
	intptr_t m_iUnknown; // checksum(?)
	intptr_t m_iUnknown2; // event version(?)	
 
}HL_Event_t;